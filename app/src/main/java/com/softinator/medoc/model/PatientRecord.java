package com.softinator.medoc.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rajendra on 3/6/17.
 */

public class PatientRecord {


    @SerializedName("result")
    public Result result;

    public static class Result {
        @SerializedName("id")
        public int id;
        @SerializedName("firstName")
        public String firstName;
        @SerializedName("lastName")
        public String lastName;
        @SerializedName("mobileNumber")
        public String mobileNumber;
        @SerializedName("userName")
        public String userName;
        @SerializedName("password")
        public String password;
        @SerializedName("enabled")
        public boolean enabled;
        @SerializedName("signupDate")
        public String signupDate;
    }
}
