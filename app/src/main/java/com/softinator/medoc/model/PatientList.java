package com.softinator.medoc.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;

/**
 * Created by rajendra on 28/5/17.
 */

public class PatientList implements Serializable{


    @SerializedName("result")
    public List<Result> result;

    public static class Result  implements Serializable{
        @SerializedName("id")
        public int id;
        @SerializedName("firstName")
        public String firstName;
        @SerializedName("lastName")
        public String lastName;
        @SerializedName("mobileNumber")
        public String mobileNumber;
        @SerializedName("userName")
        public String userName;
        @SerializedName("password")
        public String password;
        @SerializedName("enabled")
        public boolean enabled;
        @SerializedName("signupDate")
        public String signupDate;
        public int position;
    }
}
