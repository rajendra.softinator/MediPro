package com.softinator.medoc.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rajendra on 3/6/17.
 */

public class VisitList {

    @SerializedName("result")
    public List<Result> result;

    public static class Result {
        @SerializedName("id")
        public int id;
        @SerializedName("patientId")
        public int patientId;
        @SerializedName("doctorId")
        public int doctorId;
        @SerializedName("visitDate")
        public String visitDate;
        @SerializedName("location")
        public String location;
        @SerializedName("weight")
        public double weight;
        @SerializedName("height")
        public double height;
    }
}
