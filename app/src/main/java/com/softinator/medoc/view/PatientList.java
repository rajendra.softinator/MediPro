package com.softinator.medoc.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.softinator.medoc.R;
import com.softinator.medoc.view.rest.ApiClient;
import com.softinator.medoc.view.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientList extends AppCompatActivity implements PatientAdapter.OnClickPatient {
    private static final String TAG = "PatientList";
    private RecyclerView mRecyclerViewm_patientList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        mRecyclerViewm_patientList = (RecyclerView) findViewById(R.id.patientList);
        mRecyclerViewm_patientList.setLayoutManager(new LinearLayoutManager(this));

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<com.softinator.medoc.model.PatientList> call = apiService.getPatient();


        call.enqueue(new Callback<com.softinator.medoc.model.PatientList>() {
            @Override
            public void onResponse(Call<com.softinator.medoc.model.PatientList> call, Response<com.softinator.medoc.model.PatientList> response) {
                Log.i(TAG, "onResponse: Result" + response.message());
                PatientAdapter patientAdapter = new PatientAdapter(response.body().result, PatientList.this);
                mRecyclerViewm_patientList.setAdapter(patientAdapter);

            }

            @Override
            public void onFailure(Call<com.softinator.medoc.model.PatientList> call, Throwable t) {
                // Log error here since request failed
                Log.i(TAG, "onResponse: Result" + t);


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view, com.softinator.medoc.model.PatientList.Result result) {
        Intent intent = new Intent(getApplicationContext(), com.softinator.medoc.view.Patient.class);
        intent.putExtra("result",result);
        startActivity(intent);
        finish();
    }
}
