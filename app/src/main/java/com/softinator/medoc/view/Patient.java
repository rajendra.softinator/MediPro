package com.softinator.medoc.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.softinator.medoc.R;
import com.softinator.medoc.model.PatientRecord;
import com.softinator.medoc.model.VisitList;
import com.softinator.medoc.view.rest.ApiClient;
import com.softinator.medoc.view.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Patient extends AppCompatActivity {


    private static final String TAG ="PatientList" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

downloadSinglePatientRecord();
        downloadVistList();

    }

    private void downloadVistList() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<com.softinator.medoc.model.VisitList> call = apiService.getVisitList();


        call.enqueue(new Callback<com.softinator.medoc.model.VisitList>() {
            @Override
            public void onResponse(Call<com.softinator.medoc.model.VisitList> call, Response<com.softinator.medoc.model.VisitList> response) {
                Log.i(TAG, "onResponse: Result" + response.message());
                populateVisistList(response.body().result);


            }

            @Override
            public void onFailure(Call<com.softinator.medoc.model.VisitList> call, Throwable t) {
                // Log error here since request failed
                Log.i(TAG, "onResponse: Result" + t);


            }
        });

    }

    private void populateVisistList(List<VisitList.Result> result) {
        RecyclerView   recyclerView_visitList  = (RecyclerView) findViewById(R.id.visit_list);
recyclerView_visitList.setLayoutManager(new LinearLayoutManager(this));
        recyclerView_visitList.setAdapter(new VisitAdapter(result,Patient.this))







        ;

    }

    private void downloadSinglePatientRecord() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<com.softinator.medoc.model.PatientRecord> call = apiService.getSinglePatientById();


        call.enqueue(new Callback<com.softinator.medoc.model.PatientRecord>() {
            @Override
            public void onResponse(Call<com.softinator.medoc.model.PatientRecord> call, Response<com.softinator.medoc.model.PatientRecord> response) {
                Log.i(TAG, "onResponse: Result" + response.message());
                populateData(response.body().result);

            }

            @Override
            public void onFailure(Call<com.softinator.medoc.model.PatientRecord> call, Throwable t) {
                // Log error here since request failed
                Log.i(TAG, "onResponse: Result" + t);


            }
        });
    }

    private void populateData(PatientRecord.Result record)
    {
        TextView      textView_firstName  = (TextView) findViewById(R.id.first_name);
        TextView      textView_lastName = (TextView) findViewById(R.id.last_name);
        textView_firstName.setText(record.firstName);
        textView_lastName.setText(record.lastName);


    }


}
