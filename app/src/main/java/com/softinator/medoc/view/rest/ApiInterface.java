package com.softinator.medoc.view.rest;


import com.softinator.medoc.model.PatientList;
import com.softinator.medoc.model.PatientRecord;
import com.softinator.medoc.model.VisitList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by rajendraverma on 15-08-2016.
 */
public interface ApiInterface {
    @GET("/patients.json")
    Call<PatientList> getPatient();
    @GET("/10.json")
    Call<PatientRecord> getSinglePatientById();
    @GET("/visits.json")
    Call<VisitList> getVisitList();


}
