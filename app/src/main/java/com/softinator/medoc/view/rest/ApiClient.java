package com.softinator.medoc.view.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rajendraverma on 15-08-2016.
 */
public class ApiClient {

    public static final String BASE_URL = "http://34.208.99.213";
    private static Retrofit retrofit = null;
//http://34.208.99.213/10.json


    public static Retrofit getClient() {
        if (retrofit==null) {

            retrofit = new Retrofit

                    .Builder()
                    .baseUrl(BASE_URL)

                    .addConverterFactory(GsonConverterFactory.create())

                    .build();
        }


        return retrofit;
    }
}
