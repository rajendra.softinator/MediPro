package com.softinator.medoc.view;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.softinator.medoc.R;
import com.softinator.medoc.model.PatientList;

import java.util.List;

/**
 * Created by rajendra on 28/5/17.
 */

public class PatientAdapter extends RecyclerView.Adapter<PatientAdapter.PatientViewHolder> {
    private List<com.softinator.medoc.model.PatientList.Result> mPatientList;
    private Activity mActivity;
    public  OnClickPatient mOnClickPatient;

    public PatientAdapter(List<PatientList.Result> patientList, Activity activity) {
        mPatientList = patientList;
        mActivity = activity;
        mOnClickPatient= (OnClickPatient) mActivity;
    }

    @Override
    public PatientAdapter.PatientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mActivity);
        View view = layoutInflater.inflate(R.layout.patient_list, null);

        return new PatientViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PatientAdapter.PatientViewHolder holder, final int position) {
        holder.mTextView_firstName.setText(mPatientList.get(position).firstName);
        holder.mTextView_lastName.setText(mPatientList.get(position).firstName);
        mPatientList.get(position).position  = position;
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             mOnClickPatient.onClick(v,mPatientList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPatientList.size();
    }

    public class PatientViewHolder extends RecyclerView.ViewHolder  {
        private TextView mTextView_firstName;
        private TextView mTextView_lastName;
        private  View  mView;

        public PatientViewHolder(View itemView) {
            super(itemView);
            mTextView_firstName = (TextView) itemView.findViewById(R.id.first_name);
            mTextView_lastName = (TextView) itemView.findViewById(R.id.last_name);
            mView = itemView.findViewById(R.id.row);

        }

    }

    public  static  interface OnClickPatient
    {
        public void onClick(View view, PatientList.Result result);


    }
}
