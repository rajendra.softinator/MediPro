package com.softinator.medoc.view;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.softinator.medoc.model.PatientRecord;
import com.softinator.medoc.model.VisitList;
import com.softinator.medoc.R;

import java.util.List;

/**
 * Created by rajendra on 3/6/17.
 */

public class VisitAdapter extends RecyclerView.Adapter<VisitAdapter.VisitViewHolder> {
    private List<VisitList.Result> visitList;
    private Activity mActivity;

    public VisitAdapter(List<VisitList.Result> visitList, Activity activity) {
        this.visitList = visitList;
        mActivity = activity;
    }

    @Override
    public VisitAdapter.VisitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mActivity);
        View view = layoutInflater.inflate(R.layout.visit_list, null);
        return new VisitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VisitAdapter.VisitViewHolder holder, int position) {
        holder.mTextView_location.setText(visitList.get(position).location);
    }

    @Override
    public int getItemCount() {
        return visitList.size();
    }

    public class VisitViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextView_location;

        public VisitViewHolder(View itemView) {
            super(itemView);
            mTextView_location = (TextView) itemView.findViewById(R.id.location);
        }
    }
}
